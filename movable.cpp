#include <ncurses.h>
#include "movable.h"

void Movable::next_frame_move()
{
    mvaddch(y, x, ' ');
    switch(d)
    {
        case Up:
            y = y - 1;
            break;
        case Down:
            y = y + 1;
            break;
        case Left:
            x = x - 1;
            break;
        case Right:
            x = x + 1;
            break;
    }
    mvaddch(y, x, appear);
    refresh();
}

void Movable::next_frame()
{
    refresh();
    if(frame_counter >= speed)
    {
        next_frame_move();
        frame_counter = 0;
    }
    else
        frame_counter++;
}

Movable::Movable(int starty, int startx, int s, Direction dir, char a)
{
    y = starty;
    x = startx;
    speed = s;
    d = dir;
    frame_counter = 0;
    enable = true;
    appear = a;
}

Movable::~Movable()
{
    mvaddch(y, x, ' ');
    refresh();
}

void Movable::remove()
{
    mvaddch(y, x, ' ');
    refresh();
    enable = false;
}

bool Movable::get_enable() const
{
    return enable;
}
void Movable::set_xy(int _x, int _y)
{
    y = _y;
    x = _x;
}

int Movable::get_y() const
{
    return y;
}

int Movable::get_x() const
{
    return x;
}
