#include "weapon.h"
#include "movable.h"

char Weapon::get_weapon() const
{
    return weapon_appearance;
}

Weapon::Weapon(int y, int x, int speed, char weapon, char bullet) : Movable(y, x, speed, Right, bullet)
{
    weapon_appearance = weapon;
}

//AK47::AK47(int y, int x) : Weapon(y, x, 2, '=', '.') {}
