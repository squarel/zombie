#include "scene.h"
#include "weapon.h"
#include <ncurses.h>
#include <string>

using std::string;

Box::Box(int y, int x, int w, int h)
{
    start_x = x;
    start_y = y;
    height = h;
    width = w;

    border.ls = '|';
    border.rs = '|';
    border.ts = '-';
    border.bs = '-';
    border.tl = '+';
    border.tr = '+';
    border.bl = '+';
    border.br = '+';

    draw();
}

Box::~Box()
{
    int i;
    for(i = start_x; i <= (start_x + width); i++)
        mvvline(start_y, i, ' ', height + 1);
    refresh();
}

void Box::draw()
{
    mvvline(start_y + 1, start_x, border.ls, height - 1);
    mvvline(start_y + 1, start_x + width, border.rs, height - 1);
    mvhline(start_y, start_x + 1, border.ts, width - 1);
    mvhline(start_y + height, start_x + 1, border.bs, width - 1);
    mvaddch(start_y, start_x, border.tl);
    mvaddch(start_y, start_x + width, border.tr);
    mvaddch(start_y + height, start_x, border.bl);
    mvaddch(start_y + height, start_x + width, border.br);
    refresh();
}

void StatusBar::inc_score()
{
    score++;
}

void StatusBar::inc_level()
{
    level++;
}

void StatusBar::dec_life()
{
    life--;
}

int StatusBar::get_life()
{
    return life;
}

void StatusBar::set_demo(bool on)
{
    demo = on;
}

bool StatusBar::get_demo()
{
    return demo;
}

void StatusBar::set_quit_flag(bool yes)
{
    quit_flag = yes;
}

bool StatusBar::get_quit_flag()
{
    return quit_flag;
}

StatusBar::StatusBar(int starty, int startx)
{
    x = startx;
    y = starty;
    score = 0;
    level = 0;
    life = 5;
    demo = false;
}

void StatusBar::draw()
{
    std::string demo_str = demo ? "on" : "off";
    mvhline(y, x, ' ', 40);
    mvprintw(y, x, " score: %d, life: %d, demo: %s",\
            score, life, demo_str.c_str());
    refresh();
}

void Player::move_down()
{
    if(y + 1 < low_bound)
    {
        mvaddch(y, x, ' ');
        mvaddch(y, x+1, ' ');
        y++;
        mvaddch(y, x, character_appear);
        mvaddch(y, x+1, weapon->get_weapon());
        refresh();
    }
}

void Player::move_up()
{
    if(y - 1 > up_bound)
    {
        mvaddch(y, x, ' ');
        mvaddch(y, x+1, ' ');
        y--;
        mvaddch(y, x, character_appear);
        mvaddch(y, x+1, weapon->get_weapon());
        refresh();
    }
}

Player::Player(int starty, int startx, int up_b, int low_b, Weapon* wp)
{
    y = starty;
    x = startx;
    up_bound = up_b;
    low_bound = low_b;
    weapon = wp;
    mvaddch(y, x, character_appear);
    mvaddch(y, x+1, weapon->get_weapon());
    refresh();
}

void Player::change_weapon(Weapon *wp)
{
    weapon = wp;
}

int Player::get_y()
{
    return y;
}

Weapon * Player::fire()
{
    Weapon * wp = new Weapon(*weapon);
    // player itself takes two characters
    wp->set_xy(x + 2, y);
    return wp;
}

Enemy::Enemy(int l_bound, int r_bound, int starty, int speed, char appear) : Movable(starty, r_bound, speed, Left, appear)
{
    left_bound = l_bound;
    right_bound = r_bound;
}

void init_menu(Box* &b)
{
    int height = 10;
    int width = 40;
    int starty = (LINES - height) / 2;
    int startx = (COLS - width) / 2;

    b = new Box(starty, startx, width, height);
    mvprintw(starty + 2, startx + 3, "* press 'd' for demo");
    mvprintw(starty + 3, startx + 3, "* press 'p' to play");
    mvprintw(starty + 4, startx + 3, "* press others to quit");
    mvprintw(starty + 5, startx + 3, "* use ctl-c to quit in game");
    mvprintw(starty + 6, startx + 3, "* 'j' to move down");
    mvprintw(starty + 7, startx + 3, "* 'k' to move up");
    mvprintw(starty + 8, startx + 3, "* SPACE to shoot");
    refresh();
}

void init_statusbar(StatusBar* &sb)
{
    int status_bar_y = (LINES - SCENE_HEIGHT) / 2 + 1;
    int status_bar_x = (COLS - SCENE_WIDTH) / 2 + 1;

    sb = new StatusBar(status_bar_y, status_bar_x);
    sb->draw();

}

void init_scene(Box* &b)
{
    int height = 20;
    int width = 120;
    int starty = (LINES - height) / 2;
    int startx = (COLS - width) / 2;

    b = new Box(starty, startx, width, height);

    mvhline(starty + 2, startx + 1, '-', width - 1);
}

void init_player(Player* &p, int scene_left, int scene_top, int scene_bottom)
{
    int starty = LINES / 2;
    int startx = scene_left;
    Weapon* wp = new Weapon(starty, startx, 2, '=', '.');
    p = new Player(starty, startx, scene_top, scene_bottom, wp);
}
