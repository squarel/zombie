#ifndef GUARD_CONTROL_H
#define GUARD_CONTROL_H

#include "scene.h"

void repeat(map<char, map<int, vector<Movable*> > >*, int, int, int, int, StatusBar*);
void game_loop(Player*, map<char, map<int, vector<Movable*> > >*);
void collision_detect(map<char, map<int, vector<Movable*> > >*);
void demo_loop(map<char, map<int, vector<Movable*> > >*, Player*, int, int);

#endif
