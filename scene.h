#ifndef GUARD_SCENE_H
#define GUARD_SCENE_H

#include <ncurses.h>
#include <vector>
#include "weapon.h"
#include "movable.h"

using namespace std;

#define SCENE_HEIGHT 20
#define SCENE_WIDTH 120

typedef struct _box_border
{
    chtype ls, rs, ts, bs, tl, tr, bl, br;
    /* left, right, top, bottom, top left, top right, bottom left, bottom right
    of the window */
}BOX_BORDER;


class Box
{
    private:
        int start_x, start_y, height, width;
        BOX_BORDER border;

    public:
        Box(int y, int x, int width, int height);
        void draw();
        ~Box();
};

class StatusBar
{
    private:
        int x, y;
        int score, level, life;
        bool demo;
        bool quit_flag;

    public:
        StatusBar(int, int);
        void inc_score();
        void inc_level();
        int get_level();
        void dec_life();
        int get_life();
        void set_quit_flag(bool);
        bool get_quit_flag();
        void set_demo(bool);
        bool get_demo();
        void draw();
};

class Player
{
    private:
        int x, y;
        int up_bound, low_bound;
        char character_appear = 'C';
        Weapon *weapon;

    public:
        Player(int, int, int, int, Weapon*);
        void move_up();
        void move_down();
        void change_weapon(Weapon*);
        Weapon * fire();
        int get_y();
};

class Enemy: public Movable
{
    private:
        int left_bound, right_bound;

    public:
        Enemy(int l_bound, int r_bound, int starty, int speed, char appear);
};

void init_menu(Box* &);
void init_scene(Box* &);
void init_statusbar(StatusBar* &);
void init_player(Player* &, int, int, int);

#endif
