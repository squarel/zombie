#include <ncurses.h>
#include <thread>
#include <iostream>
#include <vector>
#include <chrono>
#include <map>
#include <stdlib.h>

#include "scene.h"
#include "weapon.h"
#include "movable.h"
#include "control.h"

using namespace std;

int main(int argc, char *argv[])
{
    /* initialize ncurses */
    initscr();
    clear();
    noecho();
    cbreak();
    keypad(stdscr, TRUE);
    curs_set(0);
    start_color();


    /* initialize variables */
    int scene_left = (COLS - SCENE_WIDTH) / 2 + 1;
    int scene_top = (LINES - SCENE_HEIGHT) / 2 + 2;
    int scene_bottom = (LINES + SCENE_HEIGHT) / 2;
    int scene_right = (COLS + SCENE_WIDTH) / 2 - 1;
    char ch;
    // all movables objects saved by line and type
    map<char, map<int, vector<Movable*> > > all_movables;

    /* initialize scene */
    Box *menu;
    init_menu(menu);
    ch = getch();
    StatusBar *sb;
    delete menu;
    Box *scene;
    init_scene(scene);
    init_statusbar(sb);
    Player* pl;
    init_player(pl, scene_left, scene_top, scene_bottom);

    if(ch == 'd')
    {
        sb->set_demo(true);
        sb->draw();
        thread demo(demo_loop, &all_movables, pl, scene_top, scene_bottom);
        thread first(repeat, &all_movables, scene_left + 2, scene_right, scene_top, scene_bottom, sb);
        first.join();
        demo.join();
    }
    else if(ch == 'p')
    {
        thread first(repeat, &all_movables, scene_left + 2, scene_right, scene_top, scene_bottom, sb);
        game_loop(pl, &all_movables);
        first.join();
    }

    endwin();
    return 0;
}

void demo_loop(map<char, map<int, vector<Movable*> > >* all_movables, Player* pl, int upper_bound, int lower_bound)
{
    bool direction_up = true;
    while(1)
    {
        if(direction_up)
            pl->move_up();
        else if(!direction_up)
            pl->move_down();
        Movable *new_movable = pl->fire();
        int y = new_movable->get_y();
        (*all_movables)['b'][y].push_back(new_movable);
        if(pl->get_y() <= (upper_bound + 1) && direction_up)
            direction_up = false;
        else if(pl->get_y() >= (lower_bound - 1) && !direction_up)
            direction_up = true;
        this_thread::sleep_for(chrono::milliseconds(100));
    }
}

void create_enemy(map<char, map<int, vector<Movable*> > >* all_movables, int right_bound, int upper_bound, int lower_bound)
{
    int speed = rand() % 5 + 1;
    int y = rand() % (lower_bound - upper_bound - 1) + upper_bound + 1;
    Enemy *e1 = new Enemy(100, right_bound, y, speed, '&');
    (*all_movables)['e'][y].push_back(e1);
}

void repeat(map<char, map<int, vector<Movable*> > >* all_movables, int left_bound, int right_bound, int upper_bound, int lower_bound, StatusBar* sb)
{
    int level = 0;
    while(1)
    {
        if(level >= 50)
        {
            create_enemy(all_movables, right_bound, upper_bound, lower_bound);
            level = 0;
        }
        else
            level++;

        // re-draw every movable object and boundary detection
        for(map<char, map<int, vector<Movable*> > >::iterator it_map = all_movables->begin(); it_map != all_movables->end(); ++it_map)
        {
            for(map<int, vector<Movable*> >::iterator it_map_v = it_map->second.begin(); it_map_v != it_map->second.end(); ++it_map_v)
            {
                for(vector<Movable*>::iterator it = it_map_v->second.begin(); it != it_map_v->second.end(); ++it)
                {
                    if((*it)->get_enable())
                    {
                        if(it_map->first == 'e' && (*it)->get_x() <= left_bound)
                        {
                            (*it)->remove();
                            sb->dec_life();
                            sb->draw();
                            if(sb->get_life() <= 0)
                            {
                                mvprintw(LINES / 2, COLS / 2, "Congrats! You are dead!");
                                mvprintw(LINES / 2 + 1, COLS / 2, "press ctr-c to quit");
                                refresh();
                                return;
                            }
                        }
                        else if(it_map->first == 'b' && (*it)->get_x() >= right_bound)
                        {
                            (*it)->remove();
                        }
                        else
                            (*it)->next_frame();
                        refresh();
                    }
                }
            }
        }

        // collision detection
        for(map<int, vector<Movable*> >::iterator it_map_v = (*all_movables)['b'].begin(); it_map_v != (*all_movables)['b'].end(); ++it_map_v)
        {
            for(vector<Movable*>::iterator it_b = it_map_v->second.begin(); it_b != it_map_v->second.end(); ++it_b)
            {
                int row = it_map_v->first;
                if((*it_b)->get_enable())
                {
                    for(vector<Movable*>::iterator it_e = (*all_movables)['e'][row].begin(); it_e != (*all_movables)['e'][row].end(); ++it_e)
                    {
                        if((*it_e)->get_enable())
                        {
                            if((*it_b)->get_x() == (*it_e)->get_x())
                            {
                                (*it_b)->remove();
                                (*it_e)->remove();
                                sb->inc_score();
                                sb->draw();
                                continue;
                            }
                        }
                    }
                }
            }
        }
        this_thread::sleep_for(chrono::milliseconds(10));
    }
}

void game_loop(Player* pl, map<char, map<int, vector<Movable*> > >* all_movables)
{
    char ch;
    while((ch = getch()) != KEY_F(1))
    {
        switch(ch)
        {
            case 'k':
                pl->move_up();
                break;
            case 'j':
                pl->move_down();
                break;
            case ' ':
                Movable *new_movable = pl->fire();
                int y = new_movable->get_y();
                (*all_movables)['b'][y].push_back(new_movable);
                break;
        }
    }
}
