#ifndef GUARD_MOVABLE_H
#define GUARD_MOVABLE_H

#include <ncurses.h>

enum Direction
{
    Up, Right, Down, Left
};

class Movable
{
    private:
        int y;
        int x;
        Direction d;
        int speed; //higher the slower
        int frame_counter;
        char appear;
        bool enable;

        void next_frame_move();

    public:
        Movable(int y, int x, int speed, Direction dir, char appear);
        ~Movable();
        void next_frame();
        void set_xy(int x, int y);
        int get_y() const;
        int get_x() const;
        bool get_enable() const;
        void remove();
};
#endif
