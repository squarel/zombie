#ifndef GUARD_WEAPON_H
#define GUARD_WEAPON_H

#include "movable.h"

class Weapon: public Movable
{
    private:
        char weapon_appearance;

    public:
        Weapon(int y, int x, int speed, char weapon, char bullet);
        char get_weapon() const;
};

//class AK47: public Weapon
//{
    //public:
        //AK47(int y, int x);
//}

#endif
