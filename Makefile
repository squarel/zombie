CC=g++
CFALGS=-Wall -g -std=c++11
LDFLAGS=-lncurses -lpthread
TARGET= a.out

all: $(TARGET)

$(TARGET): main.o scene.o weapon.o movable.o
	$(CC) $(CFALGS) $(LDFLAGS) -o $(TARGET) main.o scene.o weapon.o movable.o

scene.o: scene.h scene.cpp
	$(CC) $(CFALGS) -c scene.cpp

main.o: main.cpp
	$(CC) $(CFALGS) -c main.cpp

weapon.o: weapon.h weapon.cpp
	$(CC) $(CFALGS) -c weapon.cpp

movable.o: movable.h movable.cpp
	$(CC) $(CFALGS) -c movable.cpp
clean:
	rm -f *.o
